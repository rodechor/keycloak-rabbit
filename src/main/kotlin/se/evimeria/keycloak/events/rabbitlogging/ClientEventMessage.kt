package se.evimeria.keycloak.events.rabbitlogging

import org.keycloak.events.Event
import org.keycloak.events.EventType

data class ClientEventMessage(
    val clientId: String,
    val details: MutableMap<String, String>,
    val error: String,
    val ipAddress: String,
    val realmId: String,
    val sessionId: String,
    val time: Long,
    val type: EventType,
    val userId: String
) {

    companion object {
        fun create(event: Event): ClientEventMessage {
            return ClientEventMessage(event.clientId,
            event.details ,
            event.error,
            event.ipAddress,
            event.realmId,
            event.sessionId,
            event.time,
            event.type,
            event.userId)
        }
    }
}