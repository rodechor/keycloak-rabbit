package se.evimeria.keycloak.events.rabbitlogging

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.keycloak.events.Event
import org.keycloak.events.EventListenerProvider
import org.keycloak.events.admin.AdminEvent

class EventListenerProvider (val rabbit: EventRabbitConfig) :
    EventListenerProvider {
    private val mapper = jacksonObjectMapper()
    override fun close() {
    }

    override fun onEvent(event: Event) {
       logEventInMQ( mapper.writeValueAsString(ClientEventMessage.create(event)) )
    }

    override fun onEvent(event: AdminEvent, b: Boolean) {
        logEventInMQ( mapper.writeValueAsString(AdminEventMessage.create(event)) )
    }

    private fun logEventInMQ(message : String) {
        rabbit.sendMessage(message)
    }
}