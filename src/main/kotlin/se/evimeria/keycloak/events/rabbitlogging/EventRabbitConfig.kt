package se.evimeria.keycloak.events.rabbitlogging

import com.rabbitmq.client.*
import org.keycloak.Config.Scope
import se.evimeria.keycloak.rabbitmq.RabbitConfig

class EventRabbitConfig: RabbitConfig() {
    private var exchange = ""
    private var routingKey = ""

    override fun loadConfig(scope : Scope?) {
        super.loadConfig(scope)
        exchange = scope?.get("exchange")?: getEnvProperty("KEYCLOAK.RABBIT.EVENTLOG.EXCHANGE", "")
        routingKey = scope?.get("routingKey")?: getEnvProperty("KEYCLOAK.RABBIT.EVENTLOG.ROUTINGKEY", "log.system")
    }

    fun sendMessage(message: String) {
        try {
            val channel = getConnection().createChannel()
            channel?.basicPublish(exchange, routingKey, makeProperties(), message.toByteArray())
            channel?.close()
        } catch (e: Exception) {
            System.err.println("Keycloak-to-Rabbit: Error when sending to RMQ")
            e.printStackTrace()
        }
    }

    companion object {
        private fun makeProperties() : AMQP.BasicProperties {
            val propsBuilder = AMQP.BasicProperties.Builder()
                .appId("Keycloak")
                .contentType("application/json")
                .contentEncoding("UTF-8")
                .headers(mapOf("message-type" to "keyclok-event-log"))
            return propsBuilder.build()
        }
    }
}