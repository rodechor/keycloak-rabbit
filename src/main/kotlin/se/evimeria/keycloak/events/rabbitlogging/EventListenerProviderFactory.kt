package se.evimeria.keycloak.events.rabbitlogging

import org.keycloak.Config.Scope
import org.keycloak.events.EventListenerProvider
import org.keycloak.events.EventListenerProviderFactory
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory

class EventListenerProviderFactory: EventListenerProviderFactory {
    private val rabbit = EventRabbitConfig()

    override fun create(session: KeycloakSession?): EventListenerProvider {
        return EventListenerProvider(rabbit)
    }

    override fun init(scope: Scope?) {
        rabbit.loadConfig(scope)
    }

    override fun postInit(factory: KeycloakSessionFactory?) {
    }

    override fun close() {
        rabbit.close()
    }

    override fun getId() = "keycloak-to-rabbit"
}