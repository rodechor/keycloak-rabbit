package se.evimeria.keycloak.events.rabbitlogging

import org.keycloak.events.admin.AdminEvent
import org.keycloak.events.admin.AuthDetails
import org.keycloak.events.admin.OperationType
import org.keycloak.events.admin.ResourceType

data class AdminEventMessage(
    val authDetails: AuthDetails,
    val error: String,
    val operationType: OperationType,
    val realmId: String,
    val representation: String,
    val resourcePath: String,
    val resourceType: ResourceType,
    val resourceTypeAsString: String,
    val time: Long
) {

    companion object {
        fun create(event: AdminEvent): AdminEventMessage {
            return AdminEventMessage(event.authDetails,
                event.error,
                event.operationType,
                event.realmId,
                event.representation,
                event.resourcePath,
                event.resourceType,
                event.resourceTypeAsString,
                event.time)
        }
    }
}
