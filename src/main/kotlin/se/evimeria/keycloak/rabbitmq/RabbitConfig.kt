package se.evimeria.keycloak.rabbitmq

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.rabbitmq.client.*
import org.keycloak.Config.Scope

open class RabbitConfig() {
    private val connectionFactory = ConnectionFactory()
    private var connection : Connection? = null

    protected fun getEnvProperty(name : String, default : String) : String {
        return when (System.getenv(name)) {
            null -> default
            else -> System.getenv(name)
        }
    }

    open fun loadConfig(scope : Scope?) {
        connectionFactory.username = scope?.get("username")?: getEnvProperty("KEYCLOAK.RABBIT.USER", "admin")
        connectionFactory.password = scope?.get("password")?: getEnvProperty("KEYCLOAK.RABBIT.PASSWORD", "admin")
        connectionFactory.virtualHost = scope?.get("vhost")?: getEnvProperty("KEYCLOAK.RABBIT.VHOST", "")
        connectionFactory.host =          scope?.get("url")?: getEnvProperty("KEYCLOAK.RABBIT.URL", "localhost")
        connectionFactory.port = Integer.valueOf(scope?.get("port")?:getEnvProperty("KEYCLOAK.RABBIT.PORT", "5672"))
        connection?.let { it.close(); connection = null }    // Discard existing connections if any
    }

    fun close() {
        connection?.let { it.close(); connection = null }    // Discard existing connections if any
    }

    fun getConnection(): Connection {
        return connection?: connectionFactory.newConnection()
    }
}