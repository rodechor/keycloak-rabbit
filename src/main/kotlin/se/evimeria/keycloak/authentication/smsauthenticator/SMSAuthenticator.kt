package se.evimeria.keycloak.authentication.smsauthenticator

import org.keycloak.authentication.AuthenticationFlowContext
import org.keycloak.authentication.AuthenticationFlowError
import org.keycloak.authentication.Authenticator
import org.keycloak.common.util.RandomString
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.models.UserModel
import org.keycloak.theme.Theme
import javax.ws.rs.core.Response

class SMSAuthenticator() : Authenticator {

    companion object {
        const val SMS_PHONENUMBER = "phonenumber-for-sms"
        const val OTP_TOKEN_FORM = "authenticate-sms-login-token.ftl"
    }

    override fun authenticate(context: AuthenticationFlowContext) {
        val session = context.session
        val realm = context.realm
        val authConfig = context.authenticatorConfig
        val authSession = context.authenticationSession

        //val phonenumber = context.user.getFirstAttribute(SMS_PHONENUMBER)
        val length = Integer.parseInt(authConfig.config[SMSAuthenticatorFactory.TOKEN_DIGITS])
        val ttl = Integer.parseInt(authConfig.config[SMSAuthenticatorFactory.TOKEN_TTL])

        val token = RandomString.randomCode(length)
        authSession.setAuthNote("token", token)
        authSession.setAuthNote("ttl", (System.currentTimeMillis() + (ttl * 1000 * 60)).toString())

        try {
            val theme = session.theme().getTheme(Theme.Type.LOGIN)
            val locale = session.context.resolveLocale(context.user)
            val smsAuthText = token
            // TODO: Add more info to SMS message

            // TODO: Send SMS to rabbit MQ here

            context.challenge(context.form().setAttribute("realm", realm).createForm(OTP_TOKEN_FORM))
        } catch (e: Exception) {
            context.failureChallenge(AuthenticationFlowError.INTERNAL_ERROR, context.form()
                .setError("smsAuthSmsNotSent", e.message).createErrorPage(Response.Status.INTERNAL_SERVER_ERROR))
        }
    }

    override fun action(context: AuthenticationFlowContext) {
        val userResponse = context.httpRequest.decodedFormParameters.getFirst("code")
        when(validate(userResponse, context)) {
            AuthStatus.SERVER_ERROR ->
                context.failureChallenge(AuthenticationFlowError.INTERNAL_ERROR,
                    context.form().createErrorPage(Response.Status.INTERNAL_SERVER_ERROR))
            AuthStatus.EXPIRED ->
                context.failureChallenge( AuthenticationFlowError.EXPIRED_CODE,
                    context.form().setError("smsAuthCodeExpired").createErrorPage(Response.Status.BAD_REQUEST)
            )
            AuthStatus.INVALID -> {
                val execution = context.execution
                if (execution.isRequired) {
                    context.failureChallenge(
                        AuthenticationFlowError.INVALID_CREDENTIALS,
                        context.form().setAttribute("realm", context.realm)
                            .setError("smsAuthCodeInvalid").createForm(OTP_TOKEN_FORM)
                    )
                } else if (execution.isConditional || execution.isAlternative) {
                    context.attempted()
                }
            }
            AuthStatus.VALID -> context.success()
        }
    }
    private fun validate(userResponse : String?, context: AuthenticationFlowContext) : AuthStatus {
        val authSession = context.authenticationSession
        val challengeToken = authSession.getAuthNote("token")
        val ttl = authSession.getAuthNote("ttl")

        return if ((challengeToken == null) || (ttl == null)) {  // Values retrieved
            AuthStatus.SERVER_ERROR
        } else if (ttl.toLong() < System.currentTimeMillis()) {  // Check if expired
            AuthStatus.EXPIRED
        } else if (!userResponse.equals("123456")) {             // Token content invalid TODO: set to 'challengeToken' instead
            AuthStatus.INVALID
        } else {
            AuthStatus.VALID
        }
    }

    override fun requiresUser() = true
    override fun close() {}

    // Is the user configured for this particular authenticator?
    override fun configuredFor(session: KeycloakSession?, realm: RealmModel?, user: UserModel?) =
        (user?.getFirstAttribute(SMS_PHONENUMBER) != null)

    // Actions to perform if the user is NOT configured for this authenticator.
    // Requires user setup to be allowed in factory class (.isUserSetupAllowed()).
    override fun setRequiredActions(session: KeycloakSession?, realm: RealmModel?, user: UserModel?) {}
}

enum class AuthStatus {
    VALID,
    INVALID,
    EXPIRED,
    SERVER_ERROR
}