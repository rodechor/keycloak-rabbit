package se.evimeria.keycloak.authentication.smsauthenticator

import org.keycloak.Config
import org.keycloak.authentication.AuthenticatorFactory
import org.keycloak.models.AuthenticationExecutionModel.Requirement
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory

import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ProviderConfigProperty.STRING_TYPE


class SMSAuthenticatorFactory : AuthenticatorFactory {

    companion object {
        const val id = "SMS-HOTP-authenticator"
        const val TOKEN_DIGITS = "HOTP_TOKEN_DIGITS"
        const val TOKEN_TTL = "HOTP_TOKEN_DURATION"
        val instance = SMSAuthenticator()
    }

    override fun create(keycloakSession: KeycloakSession?) = instance
    override fun getDisplayType() = "SMS Authentication"
    override fun getHelpText() = "Authenticate by sending OTP via SMS"


    override fun getReferenceCategory(): String? = null
    override fun init(scope: Config.Scope?) {}
    override fun postInit(keycloakSessionFactory: KeycloakSessionFactory?) {}
    override fun close() {}
    override fun getId() = Companion.id

    // Which requirement switches can be used in the admin console for this authenticator?
    // REQUIRED, ALTERNATIVE and/or DISABLED are appropriate for main flows. CONDITIONAL is intended for subflows.
    override fun getRequirementChoices() = arrayOf(
        Requirement.REQUIRED, Requirement.ALTERNATIVE, Requirement.DISABLED)

    // Is the user allowed to configure missing information (such as phonenumber) during login?
    override fun isUserSetupAllowed() = false

    // Does this authenticator have properties that can be controlled in the admin console?
    override fun isConfigurable() = true
    override fun getConfigProperties(): List<ProviderConfigProperty?> {
        val digits = ProviderConfigProperty()
        digits.type = STRING_TYPE
        digits.name = TOKEN_DIGITS
        digits.label = "Number of digits in token"
        digits.helpText = "How many digits should the OTP challenge token have?"
        val ttl = ProviderConfigProperty()
        ttl.type = STRING_TYPE
        ttl.name = TOKEN_TTL
        ttl.label = "Time-to-live for token"
        ttl.helpText = "How many minutes should the OTP challenge token be considered valid?"
        val exchange = ProviderConfigProperty()
        exchange.type = STRING_TYPE
        exchange.name = "RABBITMQ_SMS_EXCHANGE"
        exchange.label = "RMQ Exchange for SMS"
        exchange.helpText = "Specifies where outbound SMS are handed over to RabbitMQ"
        val sender = ProviderConfigProperty()
        exchange.type = STRING_TYPE
        exchange.name = "SMS_SENDER_ID"
        exchange.label = "Sender name"
        exchange.helpText = "Sender name displayed to recipient of SMS"
        return listOf(digits, ttl, exchange, sender)
    }
}