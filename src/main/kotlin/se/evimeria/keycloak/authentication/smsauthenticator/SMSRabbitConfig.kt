package se.evimeria.keycloak.authentication.smsauthenticator

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.rabbitmq.client.*
import org.keycloak.Config.Scope
import se.evimeria.keycloak.rabbitmq.RabbitConfig

class SMSRabbitConfig: RabbitConfig() {
    private var exchange = ""
    private var routingKey = ""

    override fun loadConfig(scope : Scope?) {
        super.loadConfig(scope)
        exchange = scope?.get("exchange")?: getEnvProperty("KEYCLOAK.RABBIT.EVENTLOG.EXCHANGE", "")
        routingKey = scope?.get("routingKey")?: getEnvProperty("KEYCLOAK.RABBIT.EVENTLOG.ROUTINGKEY", "log.system")
    }

    fun sendSms(phonenumber: String, message: String, sender : String) {
        try {
            val channel = getConnection().createChannel()
            channel?.basicPublish(exchange, routingKey, makeProperties(), makeMessage(phonenumber, message, sender))
            channel?.close()
        } catch (e: Exception) {
            System.err.println("Keycloak-to-Rabbit: Error when sending to RMQ")
            e.printStackTrace()
        }
    }

    companion object {

        private fun makeMessage(phonenumber: String, message: String, sender: String) : ByteArray
        {
            return ("{\"recipient\":[\"$phonenumber\"], \"stringPayload\":\"$message\", \"charset\":\"UTF-8\"," +
                    " \"senderDisplayType\":\"ALPHANUMERIC\",\"senderDisplayValue\":\"$sender\"}").toByteArray()
        }

        private fun makeProperties() : AMQP.BasicProperties {
            val propsBuilder = AMQP.BasicProperties.Builder()
                .appId("Keycloak")
                .contentType("application/json")
                .contentEncoding("UTF-8")
                .headers(mapOf("sms-type" to "text-sms"))
            return propsBuilder.build()
        }
    }
}